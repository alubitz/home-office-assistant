#
# voice-skill-sdk
#
# (C) 2020, YOUR_NAME (YOUR COMPANY), Deutsche Telekom AG
#
# This file is distributed under the terms of the MIT license.
# For details see the file LICENSE in the top directory.
#
#
from skill_sdk import skill, Response, tell, ask
from skill_sdk.l10n import _
from skill_sdk.intents import context
from .persistenceHelper import *
from .reminder import *



@skill.intent_handler('TEAM_31_HOSA_START')
def handler() -> Response:
    """ Handler for start

    :return:        Response
    """
    state = getState()
    if state == 'UNKNOWN' or state == 'SLEEPING':
        # We check if we have enough goals and activities
        if sufficientSettings():
            # ask for GOAL
            goals = getGoals()
            activities = getActivities()
            name = getName()

            greeting = _('HOSA_MORNING') #TODO: make it time depended
            msg = _('HOSA_START').format(greeting=greeting, name=name, goal=goals[0], activity_1=activities[0], activity_2=activities[1]) + _('HOSA_START_GOAL_ASK').format(goal=goals[0])
            response = ask(msg)
            # setState('ASKING') # State asking

        else:
            # for first prototype: initialize goals and ask for GOAL - later this will be configured through UI

            print('INSUFFICENTE GOALS')
            setGoals(['Kundenanfragen beantworten', 'Team Meeting planen', 'Userinteface für HOSA erstellen'])
            setActivities(['Wäsche', 'Sport', 'Geschenke einpacken'])
            setName('Adrian')
            # ask for GOAL
            goals = getGoals()
            activities = getActivities()
            print('GOALS: {}'.format(goals))
            print('type(goals): {}'.format(type(goals)))
            name = getName()
            print(f'NAME: {name}')

            greeting = _('HOSA_MORNING') #TODO: make it time depended
            msg = _('HOSA_START').format(greeting=greeting, name=name, goal=goals[0], activity_1=activities[0], activity_2=activities[1]) + _('HOSA_START_GOAL_ASK').format(goal=goals[0])
            response = ask(msg)
            # setState('ASKING') # State asking
            #########################################################

            # TODO: send to settings
    else:
        msg = _('HOSA_WRONG_STATE')
        response = tell(msg)
        setState(state) # State does not change



    setLastIntent('TEAM_31_HOSA_START')
    # We return the response
    return response
