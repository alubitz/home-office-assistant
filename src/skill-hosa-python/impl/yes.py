#
# voice-skill-sdk
#
# (C) 2020, YOUR_NAME (YOUR COMPANY), Deutsche Telekom AG
#
# This file is distributed under the terms of the MIT license.
# For details see the file LICENSE in the top directory.
#
#
from skill_sdk import skill, Response, tell, ask
from skill_sdk.l10n import _
from skill_sdk.intents import context
from .persistenceHelper import *


@skill.intent_handler('TEAM_31_HOSA_YES')
def handler() -> Response:
    """ Handler for yes

    :return:        Response
    """
    state = getState()
    lastIntent = getLastIntent()

    if lastIntent == 'TEAM_31_HOSA_START':
        # We get a translated message
        goal = getGoals()[0]
        minutes = 45 #TODO: get from persistence
        msg = _('HOSA_START_GOAL').format(goal=goal, min=minutes)
        # We create a simple response
        response = tell(msg)
    else:
        activity = getActivities()[0]
        minutes = 45 #TODO: get from persistence
        msg = _('HOSA_START_ACTIVITY').format(activity=activity, min=minutes)
        response = tell(msg)

    setLastIntent('TEAM_31_HOSA_YES')
    # We return the response
    return response
