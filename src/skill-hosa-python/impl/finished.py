#
# voice-skill-sdk
#
# (C) 2020, YOUR_NAME (YOUR COMPANY), Deutsche Telekom AG
#
# This file is distributed under the terms of the MIT license.
# For details see the file LICENSE in the top directory.
#
#
from skill_sdk.services.persistence import PersistenceService
from skill_sdk import skill, Response, tell, ask
from skill_sdk.l10n import _
from .persistenceHelper import *


@skill.intent_handler('TEAM_31_HOSA_DONE')
def handler() -> Response:
    """ Handler for done

    :return:        Response
    """
    # if sufficientSettings():
        # We get a translated message
    goal = getGoals()[0]
    minutes = 45 #TODO: get from persistence
    msg = _('HOSA_START_AGAIN_GOAL').format(goal=goal, min=minutes)
    # We create a simple response
    response = tell(msg)
    # We return the response
    return response
