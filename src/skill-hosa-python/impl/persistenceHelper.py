'''this module offers helpers for the settings'''

from skill_sdk.services.persistence import PersistenceService



# UNKNOWN - if hosa was never started before or you need to update your settings
# WORKING - if you are working on a goal
# BREAK - if you are on a break
# SLEEPING - if you are out of office
# ASKING - if hosa asked you a question and needs to evaluate the last intent

STATES = ['UNKNOWN', 'WORKING', 'BREAK', 'SLEEPING', 'ASKING']

def _set(key, value):
    data = PersistenceService().get()
    data[key] = value
    PersistenceService().set(data)

def getState() -> str:
    """
    returns the current State of Hosa.

    can be one of the STATES
    """
    state = PersistenceService().get()['state']
    return state if state else "UNKNOWN"

def setState(state):
    """
    sets the current state
    """
    if state in STATES:
        _set('state',state)
    else:
        raise ValueError('state must be one of the following: {}'.format(STATES))

def getGoals(num_goals=3) -> list:
    """
    returns list of goals

    """
    data = PersistenceService().get()
    num_goals = data['num_goals']
    num_goals = int(num_goals) if num_goals else 0
    goals = []
    for i in range(num_goals):
        print('GET: goal{}'.format(i))
        goal = data[f'goal{i}']
        print(f'GET: goal: {goal}')
        goals.append(str(goal))
    return goals

def setGoals(goals:list):
    """
    sets the goals
    """
    for i, goal in enumerate(goals):
        print('SET: goal{}'.format(i))
        print(f'SET: goal: {goal}')

        _set(f'goal{i}',goal)
    _set('num_goals',i)

#TODO: addGoal
#TODO: remove Goal

def getActivities() -> list:
    """
    returns list of activities

    """
    data = PersistenceService().get()
    num_activities = data['num_activities']
    num_activities = int(num_activities) if num_activities else 0
    activities = []
    for i in range(num_activities):
        activity = data[f'activity{i}']
        activities.append(str(activity))
    return activities

def setActivities(activities:list):
    """
    sets the activities
    """
    for i, activity in enumerate(activities):
        _set(f'activity{i}',activity)
    _set('num_activities',i)

#TODO: add activity
#TODO: remove activity

def getName() -> str:
    """
    returns the name of the user
    """
    return PersistenceService().get()['name']

def setName(name:str):
    """
    sets the name of the user
    """
    _set('name',name)


def getLastIntent() -> str:
    """
    returns the last intent of the conversation - should always be called first in a handler
    """
    lastIntent = PersistenceService().get()
    if 'lastIntent' in lastIntent:
        return lastIntent['lastIntent']
    return "UNKNOWN"

def setLastIntent(intent):
    """
    sets and returns the last intent of the conversation - should always be called first in a handler
    """
    _set('lastIntent',intent)


def sufficientSettings(minGoals=3, minActivities=3)-> bool:
    """
    returns True if there are goals saved for that user and False otherwise
    """
    goals = getGoals()
    print('SUFFSETTINGS: {}'.format( goals))
    activities = getActivities()
    name = getName()
    return (len(goals) >= minGoals and len(activities) >= minActivities and name)