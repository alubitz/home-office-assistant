#
# voice-skill-sdk
#
# (C) 2020, YOUR_NAME (YOUR COMPANY), Deutsche Telekom AG
#
# This file is distributed under the terms of the MIT license.
# For details see the file LICENSE in the top directory.
#
#
from skill_sdk import skill, Response, tell, ask
from skill_sdk.l10n import _
from skill_sdk.intents import context
from .persistenceHelper import setLastIntent, getLastIntent


@skill.intent_handler('TEAM_31_HOSA_CHOOSE')
def handler() -> Response:
    """ Handler for choose

    :return:        Response
    """
    # We get a translated message
    setLastIntent('TEAM_31_HOSA_CHOOSE')
    msg = _('HOSA_START_GOAL')
    # We create a simple response
    response = tell(msg)
    # We return the response
    return response
