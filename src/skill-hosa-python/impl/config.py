#
# voice-skill-sdk
#
# (C) 2020, YOUR_NAME (YOUR COMPANY), Deutsche Telekom AG
#
# This file is distributed under the terms of the MIT license.
# For details see the file LICENSE in the top directory.
#
#
from skill_sdk import skill, Response, tell
from skill_sdk.l10n import _

import requests


@skill.intent_handler('TEAM_31_HOSA_CONFIG')
def handler() -> Response:
    """ Handler for config

    :return:        Response
    """
    # We get a translated message
    msg = _('HOSA_CONFIG')
    # We create a simple response
    response = tell(msg)
    # We return the response
    return response
