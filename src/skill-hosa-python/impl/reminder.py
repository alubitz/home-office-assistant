"""
Helper for reminder

payload example:
 {
   "active": true,
   "cyclicTimestamp": "R/2020-12-11T14:36:00/P1D",
   "eventType": "ALARM",
   "reminderText": "",
   "label": "Morning",
   "text": "Wäsche waschen"
 }

"""
import json


from skill_sdk.intents import context
from skill_sdk.requests import CircuitBreakerSession
from skill_sdk.config import config


RESPONSE_CODE_OK = 200
RESPONSE_NO_CONTENT = 204
RESPONSE_NOT_FOUND = 404
RESPONSE_BAD_REQUEST = 400


def build_header_with_cvi_token():
    """
    Retrieve cvi token from context and build the headers from this.
    :return: A map containing headers
    """
    token = context.tokens.get('cvi', '') if context.tokens else ''
    return {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Content-Language': context.locale['language'],
        'Authorization': f'Bearer {token}'
    }


def make_request_url(request_function, service_url, good_codes=(RESPONSE_CODE_OK,)):
    request_header = build_header_with_cvi_token()
    with CircuitBreakerSession(internal=True, good_codes=good_codes) as session:
        return request_function(service_url, session, request_header)


def make_request(request_function):
    service_url: str = config.get('event_calendar_service', 'service_url')
    response = make_request_url(request_function, service_url,
                                good_codes=(RESPONSE_CODE_OK, RESPONSE_NO_CONTENT, RESPONSE_NOT_FOUND))
    return response


def create_reminder(reminder_label: str, date: str, **kwargs):
    """
    this function will create a new alarm
    :return:
    """
    response = make_request(lambda url, session, headers: session.post(url, headers=headers,
                                                                       params={
                                                                           'event_type': 'REMINDER'},
                                                                       data=json.dumps({
                                                                           "cyclicTimestamp": date,
                                                                           "eventType": "REMINDER",
                                                                           "text": reminder_label,
                                                                           "name": "",
                                                                       })))

    return json.loads(response.text)
