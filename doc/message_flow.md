# The following message flow describes the conversation with the Home Office Assistant

```mermaid
graph TD;

  start_user[Guten Morgen Hosa, ich starte jetzt mit der Arbeit]-->start_bot[Hallo *USER*, Heute solltest du mit *GOAL* loslegen. Im Laufe des Tages kannst du auch noch *ACTIVITY* und *ACTIVITY* erledigen.];
  
  start_user -- unknown user --> ask_settings_bot[Bevor du starten kannst, musst du einige Einstellungen vornehmen. Möchtest du die Einstellungen öffnen?];
  ask_settings_bot --> settings_yes_user[Ja];
  ask_settings_bot --> settings_no_user[Nein];
  settings_no_user --> end_dialog_bot[Okay, falls du die Einstellungen öffenen möchtest sag einfach 'Hosa, kannst du bitte die einstellungen öffnen'];
  settings_yes_user --> open_settings_bot[Du kannst die Einstellungen unter dem Link in deiner Companion App öffnen. Wenn du alle Einstellungen vorgenommen hast, starte deinen Arbeitstag einfach mit 'Hallo Hosa, ich starte jetzt mit der Arbeit'];
  open_settings_user[Hallo Hosa, ich möchte gerne die Einstellungen öffnen] --> open_settings_bot;

  start_bot --> choose_goal_bot[Möchtest du jetzt mit *GOAL* anfangen?];
  choose_goal_bot --> goal_no_user[Nein] --> different_goal_bot[Nenne bitte eine andere Aufgabe.];
  different_goal_bot --> different_goal_user[*GOAL*];
  different_goal_user --> unknown_goal_bot[Diese Aufgabe kenne ich nicht];
  unknown_goal_bot --> different_goal_bot;
  different_goal_user --> start_working_bot[Deine *X* Minuten konzentrierte Arbeit an *GOAL* starten jetzt];
  choose_goal_bot --> goal_yes_user[Ja] --> start_working_bot;
  start_working_bot --> finished_goal_user[Ich habe *GOAL* erledigt.];
  finished_goal_user --> choose_goal_bot;
  start_working_bot -- nach *X* min --> time_passed_bot[Die Zeit ist um, du hast dir eine Pause verdient. Wie wäre es mit *ACTIVITY*?];
  time_passed_bot --> activity_no_user[Nein] --> choose_activity_bot[Nenne bitte eine andere Aktivität oder sage Zufall.];
  choose_activity_bot --> activity_random_user[Zufall] --> start_activity_bot[Super, dann nutze deine *Y* Minuten Pause für *ACTIVITY*. Los gehts!];
  time_passed_bot --> activity_yes_user[Ja] --> start_activity_bot;
  choose_activity_bot --> different_activity_user[*ACTIVITY*];
  different_activity_user --> choose_activity_bot;
  different_activity_user --> start_activity_bot;
  start_activity_bot -- nach *Y* min --> activity_end_bot[Deine Pause ist vorbei. Hast du *ACTIVITY* erledigt?];
  activity_end_bot --> activity_finished_yes_user[Ja / Ich habe *ACTIVITY* erledigt] --> choose_goal_bot;
  activity_end_bot --> activity_finished_no_user[Nein] --> too_bad_bot[Schade, dann vielleicht beim nächsten Mal.];
  too_bad_bot --> choose_goal_bot;

  classDef user fill:#15f571,stroke:#333,stroke-width:2px;
  classDef bot fill:#ea0a8e,stroke:#333,stroke-width:2px;

  class start_user user;
  class different_goal_user user;
  class finished_goal_user user;
  class different_activity_user user;
  class goal_no_user user;
  class activity_no_user user;
  class activity_yes_user user;
  class activity_finished_no_user user;
  class goal_yes_user user;
  class activity_finished_yes_user user;
  class activity_random_user user;
  class settings_yes_user user;
  class settings_no_user user;
  class open_settings_user user;
  

  class start_bot bot;
  class choose_goal_bot bot;
  class different_goal_bot bot;
  class start_working_bot bot;
  class time_passed_bot bot;
  class choose_activity_bot bot;
  class start_activity_bot bot;
  class activity_end_bot bot;
  class too_bad_bot bot;
  class unknown_goal_bot bot;
  class ask_settings_bot bot;
  class end_dialog_bot bot;
  class open_settings_bot bot;

```
