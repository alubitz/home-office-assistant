[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/alubitz/home-office-assistant)

# Home Office Assistant

A skill for the magenta smart speaker that helps you to organize your day during home office.

Be more productive! Be more creative! 

[Watch the preview video](https://youtu.be/hMddl-y-W60)

## Features
The Skill has the following features

**Baseline Features**

    - Reminds you to make break every now and then
    - Suggests you activities for your breaks
    - Suggests you tasks for your work time

**Optional Features**

    - Time tracking
    - Reminds you on events from your calendar and shedules breakes around it
    - Reminds you to check your mails if you reach a certain threashold

**Not Supported**

    - Make calls
    - Read Mails 
    - Answer Mails
    - Make coffee
    
# GitPod 

- GitPod starten
- `export GEVENT_SUPPORT=True` im Terminal eingeben
- `export LOG_FORMAT=human` im Terminal eingeben
- URL zu Port 4242 in [SDP Portal](https://commandcenter.voiceui.telekom.net/skill-development-portal/adminui/#/skills/detail/DEV-HOSA/config-dev) eintragen.



# Install and run locally

To install and run the skill locally the following steps need to be done:

## Install
- create virtual environment from `requirements.txt` with python >= 3.6
    ```bash
    cd src/skill-hosa-python
    python3 -m venv .venv
    source .venv/bin/activate
    pip install -r requirements.txt
    ```
- install [ngrok](https://dashboard.ngrok.com/get-started/setup)

# Run locally
- Run the skill in dev mode inside the virtual environment:
    ```bash
    python manage.py --dev run
    ```
- forward port `4242` with ngrok:
    ```bash
    ngrok http 4242
    ```

- change the URL of the skill in the dev-config to the generated url from ngrok
[SDP Portal](https://commandcenter.voiceui.telekom.net/skill-development-portal/adminui/#/skills/detail/DEV-HOSA/config-dev)


# Auto deploy to Heroku
The version in the develop branch is always autiomatically deployed to heroku. In the SDP this url can be used to test with the version on heroku: https://hosa-skill.herokuapp.com/v1/hosa/
